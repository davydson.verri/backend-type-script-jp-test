# Desafio Mutual - Node TS Developer

Olá, obrigado pelo interesse em fazer parte da nossa equipe de engenharia! 

O objetivo do desafio é entender o quanto você conhece sobre conceitos, planejamento e codificação. [Clique aqui](https://gitlab.com/davydson.verri/bushido) para ver quais são os valores e responsabilidades dos nossos devs.

## Instruções

- Crie um repositório no Github para versionar o seu código
- Utilize Type Script com Node para construir este desafio :rocket:
- Fique à vontade para escolher os frameworks, libs, banco de dados, etc.
- Terminou? [Clique aqui](https://forms.gle/FuK8P8YjMVyXVkSx8) para enviar o seu teste

## Contexto

Uma fintech já estabelecida no Brasil e pretende lançar um novo produto chamado de crédito chamado "Yoda Pay". Após a equipe de produto realizar diversas análises chegaram a seguinte conclusão, vão iniciar a construção da plataforma pela gestão de contas que deve possuir as seguintes funcionalidades:

- 1 - Criar conta
- 2 - Alterar conta
- 3 - Desativar conta
- 4 - Ativar conta
- 5 - Listar contas

Esta primeira versão do produto é apenas uma prova de conceito, então não precisamos nos preocupar com autenticação e segurança. 

Desenvolva apenas o backend pois nossa equipe de front vai cuidar do visual e da integração.

## Sua missão

Crie uma API REST com 5 endpoints para permitir que os front-devs possam implementar as 5 funcionalidades citadas anteriormente.

Segue uma sugestão para te auxiliar na criação da entidade:

**account**
|Field      |Type       |
|-----------|----------:|
|id         |id         |
|nome       |str(50)    |
|cpf        |str(12)    |
|phone      |str(15)    |
|adress     |str(30)    |
|created_at |datetime   |
|disabled_at|datetime   |

## Requisitos

- Não permitir mais de uma conta por CPF
- Não permitir nome em branco
- Permitir alterar apenas o telefone e endereço

## O que você não pode esquecer

- Definir corretamente os verbos e rotas da API usando boas práticas REST.
- Criar um arquivo README.md na raíz do projeto com instruções sobre como executar e testar o seu projeto
- Criar collection no [Postman](https://www.postman.com/downloads/) para testar o seu projeto. Exportar a collection e incluir na raís do projeto com o nome __postman.json__

## O que será um diferencial

- Criar ao menos menos um teste unitário utilizando a lib JEST
- Configurar o LINT no projeto
- Separar variáveis de ambiente em um arquivo ENV
- Utilizar o docker para subir o ambiente(app + database)

## O que será avaliado

### Como você desenvolveu

- Flexibilidade: Deve ser fácil incluir novas features, substituir ferramentas...
- Testabilidade: O código deve ser fácil de testar. [Leia mais](https://artesoftware.com.br/2019/02/09/complexidade-ciclomatica/) sobre complexidade ciclomática
- Simplicidade: O código deve ser fácil de entender. [Leia mais](https://artesoftware.com.br/2019/02/10/complexidade-cognitiva/) sobre complexidade cognitiva
- Duplicação de código: Reaproveite o máximo de código para evitar duplicações
- Vulnerabilidades: Evite deixar informações sensíveis no código (senhas, conn strings, etc)
- Bugs: Atenção aos detalhes para não permitir erros inesperados

## Isso pode ajudar

- [DDD](https://www.youtube.com/watch?v=2X9Q97u4tUg&list=PLkpjQs-GfEMN8CHp7tIQqg6JFowrIX9ve)
- [Design Patterns](https://github.com/torokmark/design_patterns_in_typescript)
- [Mongoose](https://mongoosejs.com/)
- [Type ORM](https://typeorm.io/)
- [Jest](https://jestjs.io/pt-BR/)
- [Docker Compose](https://imasters.com.br/banco-de-dados/docker-compose-o-que-e-para-que-serve-o-que-come)
- [Awesome README](https://github.com/matiassingers/awesome-readme)
